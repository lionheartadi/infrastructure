terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.65"
    }
  }

  required_version = ">= 0.14.9"
}

provider "azurerm" {
  features {}
}

data "azurerm_client_config" "current" {}

data "azurerm_resource_group" "primary_rg" {
  name = azurerm_resource_group.primary_rg.name
}

resource "azurerm_resource_group" "primary_rg" {
  name     = "${var.environementPrefix}-resources"
  location = var.location
}

data "azurerm_redis_cache" "primary_redis" {
  name                = azurerm_redis_cache.primary_redis.name
  resource_group_name = azurerm_redis_cache.primary_redis.resource_group_name
}

resource "azurerm_redis_cache" "primary_redis" {
  name                = "${var.environementPrefix}-elementredis"
  location            = azurerm_resource_group.primary_rg.location
  resource_group_name = azurerm_resource_group.primary_rg.name
  capacity            = 0
  family              = "C"
  sku_name            = "Basic"
  enable_non_ssl_port = false
  minimum_tls_version = "1.2"
  redis_configuration {
  }
}

data "azurerm_key_vault" "primary_keyvault" {
  name                = azurerm_key_vault.primary_keyvault.name
  resource_group_name = azurerm_key_vault.primary_keyvault.resource_group_name
}

resource "azurerm_key_vault" "primary_keyvault" {
  name                        = "${var.environementPrefix}-keyvault"
  location                    = azurerm_resource_group.primary_rg.location
  resource_group_name         = azurerm_resource_group.primary_rg.name
  enabled_for_disk_encryption = true
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false

  sku_name = "standard"

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [
      "Get",
    ]

    secret_permissions = [
      "Get",
    ]

    storage_permissions = [
      "Get",
    ]
  }
}

data "azurerm_container_registry" "primary_acr" {
  name                = azurerm_container_registry.primary_acr.name
  resource_group_name = azurerm_container_registry.primary_acr.resource_group_name
}

resource "azurerm_container_registry" "primary_acr" {
  name                = "${var.environementPrefixSimple}ContainerRegistry"
  resource_group_name = azurerm_resource_group.primary_rg.name
  location            = azurerm_resource_group.primary_rg.location
  sku                 = "Basic"

  identity {
    type = "UserAssigned"
    identity_ids = [
      var.apigateway_assigned_identity_acr_pull
    ]
  }
}

data "azurerm_servicebus_namespace" "primary_servicebus" {
  name                = azurerm_servicebus_namespace.primary_servicebus.name
  resource_group_name = azurerm_resource_group.primary_rg.name
}
resource "azurerm_servicebus_namespace" "primary_servicebus" {
  name                = "${var.environementPrefix}-servicebus-namespace"
  location            = azurerm_resource_group.primary_rg.location
  resource_group_name = azurerm_resource_group.primary_rg.name
  sku                 = "Basic"
}

module "DomainAlpha" {
  source = "./modules/DomainOptionAlpha"
}

module "apigateway" {
  source = "./modules/storage_account"
 
  storage_account_name = var.storage_account_name
  resource_group_name  = azurerm_resource_group.rg.name
  location             = azurerm_resource_group.rg.location
}

 

