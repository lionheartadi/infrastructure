variable "location" {
    type = string
    default = "westeurope"
 }

 variable "environementPrefix" {
   type = string
   default = "dev-el"
 }

 variable "environementPrefixSimple" {
   type = string
   default = "devEl"
 }

 variable "apigateway_assigned_identity_acr_pull" {
    type = string
 }


 locals {
   tfPrimaryGroup  = "primary"
 }

 
variable "localdev_mode" {
  type    = bool
  default = false
}
 