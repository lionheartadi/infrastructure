data "azurerm_client_config" "current" {}

data "azurerm_resource_group" "apigateway_rg" {
  name = azurerm_resource_group.apigateway_rg.name
}

resource "azurerm_resource_group" "apigateway_rg" {
  name     = "${var.resourcePrefix}-resources"
  location = var.location
}

data "azurerm_key_vault" "apigateway_keyvault" {
  name                = azurerm_key_vault.apigateway_keyvault.name
  resource_group_name = azurerm_key_vault.apigateway_keyvault.resource_group_name
}

resource "azurerm_key_vault" "apigateway_keyvault" {
  name                        = "${var.resourcePrefix}-keyvault"
  location                    = azurerm_resource_group.apigateway_rg.location
  resource_group_name         = azurerm_resource_group.apigateway_rg.name
  enabled_for_disk_encryption = true
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false

  sku_name = "standard"

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [
      "Get",
    ]

    secret_permissions = [
      "Get",
    ]

    storage_permissions = [
      "Get",
    ]
  }
}

data "azurerm_user_assigned_identity" "apigateway_assigned_identity_acr_pull" {
 provider            = azurerm.acr_sub
 name                = "APIGateway_User_ACR_pull"
 resource_group_name = azurerm_resource_group.apigateway_rg.name
}

resource "azurerm_app_service_plan" "apigateway_service_plan" {
 name                = "${var.resourcePrefix}-apigateway-service-plan"
 location            = var.location
 resource_group_name = azurerm_resource_group.apigateway_rg.name
 kind                = "Linux"
 reserved            = true

 sku = {
    tier = "Free"
    size = "F1"
  }
}

resource "azurerm_app_service" "apigateway_unifiedapi_app_service_container" {
 name                    = "${var.resourcePrefix}-unifiedapi-app-service-container"
 location                = var.location
 resource_group_name     = azurerm_resource_group.apigateway_rg.name
 app_service_plan_id     = azurerm_app_service_plan.apigateway_service_plan.id
 https_only              = true
 client_affinity_enabled = true
 site_config {
   scm_type  = "VSTSRM"
   always_on = "false"

   linux_fx_version  = "DOCKER|arc01.azurecr.io/myapp:latest" #define the images to usecfor you application

   health_check_path = "/health" # health check required in order that internal app service plan loadbalancer do not loadbalance on instance down
 }

 identity {
   type         = "SystemAssigned, UserAssigned"
   identity_ids = [data.azurerm_user_assigned_identity.apigateway_assigned_identity_acr_pull.id]
 }

 app_settings = local.env_variables 
}

resource "azurerm_app_service_slot" "apigateway_app_service_container_staging" {
 name                    = "${var.resourcePrefix}-staging"
 app_service_name        = azurerm_app_service.apigateway_unifiedapi_app_service_container.name
 location                = var.location
 resource_group_name     = azurerm_resource_group.apigateway_rg.name
 app_service_plan_id     = azurerm_app_service_plan.apigateway_service_plan.id
 https_only              = true
 client_affinity_enabled = true
 site_config {
   scm_type          = "VSTSRM"
   always_on         = "false"
   health_check_path = "/login"
 }

 identity {
   type         = "SystemAssigned, UserAssigned"
   identity_ids = [data.azurerm_user_assigned_identity.apigateway_assigned_identity_acr_pull.id]
 }

 app_settings = local.env_variables
}

resource "azurerm_application_insights" "apigateway_app_insight" {
 name                = "${var.resourcePrefix}-app-insight"
 location            = var.location
 resource_group_name = azurerm_resource_group.apigateway_rg.name
 application_type    = "Node.JS" # Depends on your application
 disable_ip_masking  = true
 retention_in_days   = 730
}