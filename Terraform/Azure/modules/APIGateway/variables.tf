variable "location" {
  type    = string
  default = "westeurope"
}

variable "environementPrefix" {
  type    = string
  default = "dev-el"
}

variable "domainPrefix" {
  type    = string
  default = "apigateway"
}

variable "resourcePrefix" {
  type    = string
  default = "dev-el-apigateway"
}


locals {
  tfPrimaryGroup = "primary"
  env_variables = {
    DOCKER_REGISTRY_SERVER_URL      = "https://arc01.azurecr.io"
    DOCKER_REGISTRY_SERVER_USERNAME = "ACR01"
    DOCKER_REGISTRY_SERVER_PASSWORD = "**************"
  }
  AZURE_MONITOR_INSTRUMENTATION_KEY = azurerm_application_insights.apigateway_app_insight.instrumentation_key
}


 
