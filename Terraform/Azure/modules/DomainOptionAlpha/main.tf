/* terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.65"
    }
  }

  required_version = ">= 0.14.9"
}

provider "azurerm" {
  features {}
} */

data "azurerm_client_config" "current" {}

data "azurerm_resource_group" "domain_alpha_rg" {
  name = azurerm_resource_group.domain_alpha_rg.name
}

resource "azurerm_resource_group" "domain_alpha_rg" {
  name     = "${var.resourcePrefix}-resources"
  location = var.location
}

data "azurerm_key_vault" "domain_alpha_keyvault" {
  name                = azurerm_key_vault.domain_alpha_keyvault.name
  resource_group_name = azurerm_key_vault.domain_alpha_keyvault.resource_group_name
}

resource "azurerm_key_vault" "domain_alpha_keyvault" {
  name                        = "${var.resourcePrefix}-keyvault"
  location                    = azurerm_resource_group.domain_alpha_rg.location
  resource_group_name         = azurerm_resource_group.domain_alpha_rg.name
  enabled_for_disk_encryption = true
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false

  sku_name = "standard"

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [
      "Get",
    ]

    secret_permissions = [
      "Get",
    ]

    storage_permissions = [
      "Get",
    ]
  }
}