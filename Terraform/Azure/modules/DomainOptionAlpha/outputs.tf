output "domainAlpha_key_vault_uri" {
  value = data.azurerm_key_vault.domain_alpha_keyvault.vault_uri
}

output "domainAlpha_resource_group_name" {
  value = data.azurerm_resource_group.domain_alpha_rg.name
}