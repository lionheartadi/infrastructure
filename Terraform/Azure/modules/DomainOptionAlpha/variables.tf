variable "location" {
    type = string
    default = "westeurope"
 }

 variable "environementPrefix" {
   type = string
   default = "dev-el"
 }

  variable "domainPrefix" {
   type = string
   default = "alpha"
 }

 variable "resourcePrefix" {
   type = string
   default = "dev-el-alpha"
 }


 locals {
   tfPrimaryGroup  = "primary"
 }
 