terraform {
    /*
        This definition is added so that we can access configurations and state data for Terraform Azure deployments:
        https://www.terraform.io/docs/language/settings/backends/azurerm.html

        Leave this empty to specify the needed params as environmental variables:
        TF_BACKEND_RES_GROUP
        TF_BACKEND_STORAGE_ACC
        TF_BACKEND_CONTAINER
        TF_BACKEND_KEY
    */
    backend "azurerm" {} # 
}