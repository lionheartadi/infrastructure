#!/bin/bash
if [ -z "${TF_BACKEND_KEY}"];
then
    echo "Environmental Variables for Terraform State are OK"
else
     echo "Environmental Variables for Terraform State are NOT OK, Setting them before continuing"
     source ./SetEnvironmenVariablesTerraform.sh
fi

printenv TF_BACKEND_RES_GROUP
printenv TF_BACKEND_STORAGE_ACC
printenv TF_BACKEND_CONTAINER
printenv TF_BACKEND_KEY

cd ../Azure
pwd
echo $$
terraform init -migrate-state \
-backend-config="resource_group_name=$TF_BACKEND_RES_GROUP" \
-backend-config="storage_account_name=$TF_BACKEND_STORAGE_ACC" \
-backend-config="container_name=$TF_BACKEND_CONTAINER" \
-backend-config="key=terraform.tfstate" \
-backend-config="access_key=$TF_BACKEND_KEY" \

terraform validate
terraform apply


