## Element Project Infrastructure Code

** This repository is responsible for setting up the infrastructure of the Element project. **  
Using Terraform for Infrastructure on Azure.   

### Usage cases  

#### Usage case 1: Automatic Terraform deployment  
This will automatically deploy changes to Azure once you commit and push something to the main branch.  

Check the bitbucket-pipelines.yml for more info on how the automatic deployment works.  

#### Usage case 2: Manual deployment  

This is mostly used for local development.

In the Terraform/DevTools folder you will find a few scripts that will help you:  
1. TerrafromApply.sh => Applys the changes to Azure  
2. TerrafromValidate.sh => Validates that the changes to Azure are OK and what would be done.  

Notice: You need to set some environmental variables for the script to work and for Terraform to use the correct state that is to be located in an Azure storage account, for this check the SetEnvironmenVariablesTerraform.sh script.